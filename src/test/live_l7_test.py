import unittest

# Import unit under test
from app.app_interface import WeatherInterface

class TestLiveStream(unittest.TestCase):
    def setUp(self):
        self.weather_if = WeatherInterface()

   # def test_get_windspeed_kmh (self):
      #  self.assertEqual(speed, "15")
    ##    speed = self.weather_if.get_windspeed("kmh")

    def test_get_windspeed_miles (self):
        speed = self.weather_if.get_windspeed("mph")
        self.assertEqual(speed, "20")

    def test_get_windspeed_null (self):
        speed = self.weather_if.get_windspeed(None)
        self.assertEqual(speed, "20")

    def test_get_humidity (self):
        humidity = self.weather_if.get_humidity("asdasd", "test")
        self.assertEqual(humidity, "151")