from concurrent import futures

import grpc
import grpc_helpers.weatherStation_pb2 as weatherStation_pb2
import grpc_helpers.weatherStation_pb2_grpc as weatherStation_pb2_grpc
from app.app_interface import WeatherInterface

class WeatherStation(weatherStation_pb2_grpc.WeatherStationServicer):
    def __init__(self):
      self.iface = WeatherInterface()

    def get_windspeed(self, request, context):
      print("Got request " + str(request))
      #Call the right method
      windspeed = self.iface.get_windspeed(request.unit)
      return weatherStation_pb2.WindspeedOutput(speed=windspeed)

    def get_humidity(self, request, context):
      print("Got request " + str(request))
      #Call the right method
      humidity = self.iface.get_humidity(request.unit, request.decimals)
      return weatherStation_pb2.HumidOutput(humidity=humidity)

    def getRandomNumber (self, request, context):
      print("Got request getRandomNumber")
      random = self.iface.get_random_number()
      return weatherStation_pb2.RandomNumberMessage(random=random)

def server():
   server = grpc.server(futures.ThreadPoolExecutor(max_workers=2))
   weatherStation_pb2_grpc.add_WeatherStationServicer_to_server(WeatherStation(), server)
   server.add_insecure_port('[::]:50051')
   print("gRPC starting")
   server.start()
   server.wait_for_termination()
server()