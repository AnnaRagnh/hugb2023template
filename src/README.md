### Overview

The src folder contains two applications: the server (contained in the app folder) and the client (contained in the client folder). Both use the generated grpc libraries (in grpc_helpers).

All commands below are executed from the src folder.

### Running the applications

The server is run using ``python3 -m app.server``. If you just execute the server file directly, it will fail with module not found errors, as the other files it depends on (e.g., app_interface) are not on the PYTHONPATH.

After the server is running, call the client in another terminal window using ``python3 -m client.client``.

### Adding RPCs to the server

To add new RPCs (basically remote function calls) to the server, you need to: 

1. Change the proto file in the proto folder by adding a new rpc and potentially new message types (that correspond to the parameters and return types you need)
2. Re-generate the grpc helpers using ```python3 -m grpc_tools.protoc -I ./proto --python_out=./grpc_helpers --grpc_python_out=./grpc_helpers weatherStation.proto```
This places the resulting Python files directly in the grpc_helpers folder. If you don't want your old files to be overriden, better change the folders accordingly in the command.
3. Fix the import in ``grpc_helpers/weatherStation_pb2_grpc.py`` to point to ``grpc_helpers.weatherStation_pb2`` instead of ``weatherStation_pb2``.
4. Add a function to the ``WeatherStation`` class in ``app/server.py`` with the same name as you added in the proto file. For it to work, you also need to return the right message type (as defined in the proto file).
5. Finally, add a corresponding call to the client application to see that it works.

### Commands for running unit tests and coverage

```python -m unittest test.live_l7_test #runs live_l7_test.py```

if you have issues running the unit tests, you might have to modify your PYTHONPATH variable to include the current directory. This is how it works on MAC os (and should be the same on Linux):
```export PYTHONPATH="$PYTHONPATH:."```

Some commands for coverage:

```coverage erase #erases the coverage file```

```coverage run -m unittest test.live_l7_test #runs the tests as above, but also coverage analysis over the test "live_l7_test.py"```

```coverage run --append -m unittest test.live_l7_test #same as above, but does not override previous coverage information```

```coverage run --source app -m unittest test.live_l7_test #Only collects coverage information about the code in app/```

```coverage report #shows textual coverage summary```

```coverage html #generates an HTML coverage report```

Docs for all two used libraries in Lecture 07:
<https://coverage.readthedocs.io/en/latest/cmd.html#cmd-combining>
<https://docs.python.org/3/library/unittest.html>