import grpc

import grpc_helpers.weatherStation_pb2 as weatherStation_pb2
import grpc_helpers.weatherStation_pb2_grpc as weatherStation_pb2_grpc

def run():
   with grpc.insecure_channel('localhost:50051') as channel:
      stub = weatherStation_pb2_grpc.WeatherStationStub(channel)
      response = stub.get_windspeed(weatherStation_pb2.ControlInput(unit='kmh'))
      response2 = stub.get_humidity(weatherStation_pb2.HumidInput(unit='kmh', decimals='5'))
      response3 = stub.getRandomNumber(weatherStation_pb2.Empty())
   print("Received following from server: " + response.speed)
   print("Received following from server: " + response2.humidity)  
   print("Received following from server: ", response3.random)      
run()